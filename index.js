var express = require('express'),
    app = new express(),
    fs = require('fs');
//config = require('config'),

app.use('/assets', express.static(__dirname + '/assets'));

app.get('/fredos', function(req, res){
    fs.readFile('fixtures/fredos.js', function(err, data){
        if (err) throw err
        var fredos = JSON.parse(data);

        for (var i = 0, len = fredos.length; i < len; i++){
            fredos[i].image_url = 'http://localhost:8181/assets/thumb_' + fredos[i].id + '.png';
        }
        res.send(JSON.stringify(fredos));
    });
});

app.get('/addons', function(req, res){
    fs.readFile('fixtures/addons.js', function(err, data){
        if (err) throw err;
        res.send(JSON.stringify(JSON.parse(data)));
    });
});

app.listen('8181');
console.log('Server running at http://localhost:8181');




