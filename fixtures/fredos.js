[
    {
        "id": 1,
        "city": "New York",
        "state": "NY",
        "description": "Lorem ipsum.....",
        "image_url": "http://....",
        "title": "Find me!",
        "distance": 867,
        "sponsor": "Fredo",
        "prize": {
            "prize_type": "cash",
            "prize_value": "1-5",
            "prize_description": "Lorem ipsum"
        }

    },
    {
        "id": 2,
        "city": "New York",
        "state": "NY",
        "description": "Lorem ipsum.....",
        "image_url": "http://....",
        "title": "Wooooo",
        "distance": 10922,
        "sponsor": "Best Of The Wurst",
        "prize": {
            "prize_type": "coupon",
            "prize_value": "1",
            "prize_description": " ipsum"
        }
    },
    {
        "id": 3,
        "city": "New York",
        "state": "NY",
        "description": "Lorem ipsum.....",
        "image_url": "http://....",
        "title": "So hidden",
        "distance": 8997,
        "sponsor": "Dunkin Donuts",
        "prize": {
            "prize_type": "gift_card",
            "prize_value": "3",
            "prize_description": "Lorem ipsum"
        }
    }
]