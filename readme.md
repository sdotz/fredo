#Fredo test API

You will need node.js installed to run the API on your computer. Get it here: http://nodejs.org/

Once installed open a terminal and get to this directory:

    cd /path/to/this/dir
    
    
Install the dependencies. 

    npm install
    
Then run the server

    node index.js
    
The server will start and be running. You should see

    Server running at http://localhost:8181
    
    
Try these endpoints:

http://localhost:8181/fredos
http://localhost:8181/addons

You can modify the data the endpoints provide in the files in the fixtures directory.

If you change any server code you'll have to restart the server. Ctrl+C in the terminal, then `node index.js` to start it again. 