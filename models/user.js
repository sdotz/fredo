var user = {
    fav_fredos: [],
    found_fredos: [fredo, fredo],
    username: "",
    password: "",
    oath_token: "",
    addons: [

    ]
};

var fredo = {
    location: {
        city: "",
        state: "",
        location: "midtown", //General location tag
        lat: "",
        lon: ""
    },
    prim_img: "", //Image for the table cell (cropped) and full when in detail view
    prize_icon: "", //Image url or enum?
    created: null, //Datetime
    updated: null, //Datetime
    posted: null, //Datetime
    active: true, //Found or not
    enabled: true, //Is this visible
    usage_limit: 10, //How many times can be scanned.
    times_found: 0,
    text_hint: [
        "Main hint, everyone sees this",
        "More hints, pay for them...",
        "More hints, pay for them..."
    ],
    prize_description: "",
    find_image:  "", //Image of the fredo sticker to find,
    serial: "", //QR Data
    sponsor: {
        text: "",
        url: "",
        image: ""
    }
};

var addons = {
    //Hot cold
    //Fredar finds nearest fredo
    //Gps gives approx distance from fredo
};

